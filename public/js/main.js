// Preloader
$(window).on('load', function() { // makes sure the whole site is loaded
  $('#status').fadeOut(); // will first fade out the loading animation
  $('#preloader').delay(550).fadeOut('slow'); // will fade out the white DIV that covers the website.
  $('body').delay(550).css({'overflow':'visible'});
});

// open navigatio bar
$(document).ready(function(){
	$('.menu_icon').on("click", function(){
		$('.navbar').addClass('open_navbar');
	});
	$('.close_btn').on("click", function(){
		$('.navbar').removeClass('open_navbar');
	});

  $("#home_slider").owlCarousel({
    items: 1,
    singleItem: true,
    itemsScaleUp : true,
    dots: true,
    slideSpeed: 500,
    autoPlay: 4000,
    transitionStyle: "fade",
    stopOnHover: false,
    mouseDrag  : false
  });

  //homepage grid
  	$(function(){
	  $('#projects').mixItUp();
	});

	// $(function(){
	//   $('#projects_inner').mixItUp({});
	// });
	$(function () {
	    var filterList = {
	      init: function () {
	        $('#projects_inner').mixItUp({
	          selectors: {
	            target: '.mix',
	            filter: '.filter' 
	          },
	          load: {
	            filter: 'all'  
	          }     
	        });               
	      }
	    };
	    filterList.init();
	  });

	$(function(){
	  $('.project_inner').mixItUp();
	});

  // Submenu Show
	$('.submenu').slideUp();
	$('.dropdown').on('click', function(){
		$('.submenu').slideUp();
		//alert($('.sub_' + sub_drop).css('display') !== 'none');
		//$(this).removeClass('dropdown');
		$('.dropdown').removeClass('drop');
		var sub_drop = $(this).attr('id');

		if(!$('.sub_' + sub_drop).is(':visible')) {
			$('.sub_' + sub_drop).slideDown();
			$('.sub_' + sub_drop).parent().addClass('drop');
		}
		else{
			$('.sub_' + sub_drop).parent().removeClass('drop');			
		}
	});
});