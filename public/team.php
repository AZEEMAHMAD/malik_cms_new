<!DOCTYPE html>
<html>
<head>
	<title>The Team | Malik</title>
	
	<?php include_once('includes/header.php'); ?>

	<section class="">
		<div class="">
			<img src="images/team/banner_team.jpg" alt="">
		</div>
	</section>

	<section class="inner_section">
		<div class="container">
			<div class="col-md-12 text-center team_header">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>
			</div>
		</div>
		<hr>
	</section>

	<section class="team-wrap">
		<div class="container">
			<div class="col-md-12">
				 <h3 class="team-head">THE TEAM BEHIND Malik</h3>
			</div>
			<div class="text-center col-md-10 col-md-offset-1">
				<p>
				Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Book.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Book.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Book.Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. 
			</div>
		</div>

	</section>

	<section class="inner_section">
		<div class="container">
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
			<div class="col-md-3">
				<div class="team_image">
					<img src="images/team/2.jpg">
				</div>
				<div class="team_name">
					<h4>Fullname</h4>
					<h5>Designation</h5>
				</div>
			</div>
		</div>


	</section>

	
	<?php include_once('includes/footer.php'); ?>
	
</body>
</html>