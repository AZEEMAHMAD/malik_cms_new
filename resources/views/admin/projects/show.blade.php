@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">Project {{ $project->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/projects') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/projects/' . $project->id . '/edit') }}" title="Edit Project">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/projects' . '/' . $project->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Project"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $project->id }}</td>
                                </tr>
                                <tr>
                                    <th>Work Type</th>
                                    <td> {{ ucwords($project->works) }} </td>
                                </tr>
                                <tr>
                                    <th> Category</th>
                                    <td> {{ $project->category }} </td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $project->name }} </td>
                                </tr>
                                <tr>
                                    <th> Project Description</th>
                                    <td> {{ $project->project_description }} </td>
                                </tr>
                                <tr>
                                    <th> Banner</th>
                                    <td><img src="{{asset('ProjectBanner/'.$project->banner)}}"  height="300" width="400"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
