<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Project</b></div>
        <br/>

        <div class="form-group {{ $errors->has('works') ? 'has-error' : ''}}">
            <label for="works" class="col-md-2 control-label">{{ 'Type of Work' }} :</label>

            <div class="col-md-8">
                <select name="works" class="form-control" id="works" required>
                    <option value="">Select Type of works *</option>
                    @foreach (json_decode('{"architecture": "Architecture", "commercial": "Commercial","interior_design":"Interior Design","planning":"Planning"}', true) as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($project->works) && $project->works == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('works', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
            <label for="category" class="col-md-2 control-label">{{ 'Category' }} :</label>

            <div class="col-md-8">
                <select name="category" class="form-control" id="category" required>
                    @foreach (json_decode('{"educational": "Educational", "commercial": "Commercial","hospitality":"Hospitality","research":"Research","residential":"Residential","healthcare":"Healthcare"}', true) as $optionKey => $optionValue)
                        <option value="{{ $optionKey }}" {{ (isset($project->category) && $project->category == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
                {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label for="name" class="col-md-2 control-label">{{ 'Name' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="name" type="text" id="name"
                       value="{{ isset($project->name) ? $project->name : ''}}" required>
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('project_description') ? 'has-error' : ''}}">
            <label for="project_description" class="col-md-2 control-label">{{ 'Project Description' }} :</label>

            <div class="col-md-8">
                <textarea class="form-control" rows="5" name="project_description" type="textarea"
                          id="project_description"
                          required>{{ isset($project->project_description) ? $project->project_description : ''}}</textarea>
                {!! $errors->first('project_description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('banner') ? 'has-error' : ''}}">
            <label for="banner" class="col-md-2 control-label">{{ 'Banner' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="banner" type="file" id="banner"
                       value="{{ isset($project->banner) ? $project->banner : ''}}">
                {!! $errors->first('banner', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('images') ? 'has-error' : ''}}">
            <label for="images" class="col-md-2 control-label">{{ 'Images' }} :</label>

            <div class="col-md-8">
                <input class="form-control" name="images[]" type="file" id="images"
                       value="{{ isset($project->images) ? $project->images : ''}}" multiple>
                {!! $errors->first('images', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
