@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">Projectimage</div>
                    <div class="panel-body">
                        <a href="{{ url('/admin/project-image/create') }}" class="btn btn-success btn-sm"
                           title="Add New ProjectImage">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/project-image', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Project Name</th>
                                    <th>Filename</th>
                                    <th>Thumbnails</th>
                                    <th>Category</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($projectimage as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ isset($item->project->name)?$item->project->name:'' }}</td>
                                        <td><a href="{{asset('ProjectImages/'.$item->filename)}}" target="_blank"><img src="{{asset('ProjectImages/'.$item->filename)}}"  height="120" width="120"></a></td>
                                        <td>{{ $item->thumbnails }}</td>
                                        <td>{{ $item->category_id }}</td>
                                        <td>
                                            <a href="{{ url('/admin/project-image/' . $item->id) }}"
                                               title="View ProjectImage">
                                                <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                       aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                            <a href="{{ url('/admin/project-image/' . $item->id . '/edit') }}"
                                               title="Edit ProjectImage">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Edit
                                                </button>
                                            </a>

                                            <form method="POST"
                                                  action="{{ url('/admin/project-image' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-xs"
                                                        title="Delete ProjectImage"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $projectimage->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
