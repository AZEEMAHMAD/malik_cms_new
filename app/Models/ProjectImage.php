<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class ProjectImage extends Model
{
    use SoftDeletes;
    /* The database table used by the model.
    *
    * @var string
    */
    use  HasRoles;
    protected $table = 'project_images';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['filename', 'thumbnails', 'category_id', 'project_id'];

    public function project(){
        return $this->belongsTo('App\Models\Project','project_id');
    }

}
