<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Project extends Model
{
     use SoftDeletes;
     /* The database table used by the model.
     *
     * @var string
     */
     use  HasRoles;
    protected $table = 'projects';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category', 'name', 'project_description', 'banner', 'slug','works'];

    public function projectimages(){
        return $this->hasMany('App\Models\ProjectImage','project_id');
    }

    
}
